output "service_name" {
  value = aws_ecs_service.service.name
}

output "service_task_role_name" {
  description = "The ARN for service task role"
  value       = aws_iam_role.task_role.name
}

output "execution_role_name" {
  description = "The ARN for service execution role"
  value       = aws_iam_role.execution_role.name
}

output "sg_id" {
  value = aws_security_group.ecs_default_sg.id
}
