resource "aws_ecs_service" "service" {
  name                              = var.service_name
  cluster                           = var.cluster_name
  platform_version                  = var.platform_version
  task_definition                   = aws_ecs_task_definition.task.arn
  desired_count                     = var.desired_count
  health_check_grace_period_seconds = length(var.load_balancers) != 0 ? var.health_check_grace_period_seconds : null

  network_configuration {
    security_groups  = compact(concat([aws_security_group.ecs_default_sg.id], var.additional_security_group_ids))
    subnets          = var.subnet_ids
    assign_public_ip = var.assign_public_ip
  }

  dynamic "load_balancer" {
    for_each = var.load_balancers
    content {
      container_name   = load_balancer.value.container_name
      container_port   = load_balancer.value.container_port
      target_group_arn = load_balancer.value.target_group_arn
    }
  }

  dynamic "capacity_provider_strategy" {
    for_each = var.capacity_providers
    content {
      capacity_provider = capacity_provider_strategy.value.capacity_provider
      weight            = capacity_provider_strategy.value.weight
      base              = capacity_provider_strategy.value.base
    }
  }

  service_registries {
    registry_arn = aws_service_discovery_service.registry.arn
  }

  lifecycle {
    ignore_changes = [
      capacity_provider_strategy // This is an unfortunate workaround, otherwise terraform tries to destroy and recreate the service on each run
    ]                            // https://github.com/terraform-providers/terraform-provider-aws/issues/11351
  }

  tags = merge(local.common_tags, var.extra_tags)
}

resource "aws_ecs_task_definition" "task" {
  family                   = "${lower(replace(var.name, "/\\s/", "-"))}-${var.service_name}"
  container_definitions    = var.container_definitions
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.task_cpu
  memory                   = var.task_memory
  execution_role_arn       = aws_iam_role.execution_role.arn
  task_role_arn            = aws_iam_role.task_role.arn
  tags                     = merge(local.common_tags, var.extra_tags)
}

resource "aws_service_discovery_service" "registry" {
  name = var.service_name

  dns_config {
    namespace_id   = var.discovery_namespace_id
    routing_policy = "MULTIVALUE"

    dns_records {
      ttl  = 10
      type = "A"
    }
  }

  health_check_custom_config {
    failure_threshold = 1
  }

  tags = merge(local.common_tags, var.extra_tags)
}

resource "aws_security_group" "ecs_default_sg" {
  name        = "${lower(replace(var.name, "/\\s/", "-"))}-${lower(var.service_name)}-ecs"
  description = "${var.name} ${var.service_name}"
  vpc_id      = var.vpc_id
  tags = merge(
    {
      Name = "${var.name} ${var.service_name} ECS"
    },
    local.common_tags,
    var.extra_tags
  )
}
