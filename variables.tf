variable "name" {
  description = "Common name, a unique identifier"
  type        = string
}

variable "cluster_name" {
  type = string
}

variable "platform_version" {
  description = "Platform version. It's strongly recommended to provide a meaningful value as using LATEST may result in unexpected upgrade"
  type        = string
  default     = "LATEST"
}

variable "vpc_id" {
  description = "ID of an existing VPC"
  type        = string
}

variable "subnet_ids" {
  description = "Subnet IDs to run the tasks in"
  type        = list(string)
}

variable "service_name" {
  type = string
}

variable "container_definitions" {
  description = "Path to json-formatted definition of task containers"
  type        = string
}

variable "task_cpu" {
  type = number
}

variable "task_memory" {
  type = number
}

variable "desired_count" {
  type    = number
  default = 1
}

variable "health_check_grace_period_seconds" {
  type    = number
  default = 30
}

variable "load_balancers" {
  description = "List of objects containing load balancer definitions. Each object must contain container_name, container_port and target_group_arn"
  type = list(object(
    {
      container_name   = string
      container_port   = number
      target_group_arn = string
    }
  ))
  default = []
}

variable "capacity_providers" {
  description = "List of objects containing capacity provider definitions. Each object must contain capacity_provider name, weight and base"
  type = list(object(
    {
      capacity_provider = string
      weight            = number
      base              = number
    }
  ))
  default = []
}

variable "discovery_namespace_id" {
  type = string
}

variable "assign_public_ip" {
  description = "Whether or not public IP address must be assigned to a task"
  type        = bool
  default     = false
}

variable "additional_security_group_ids" {
  description = "Any additional Security Group IDs to attach to the service"
  type        = list(string)

  validation {
    condition     = length(var.additional_security_group_ids) <= 4
    error_message = "Only 5 security groups are allowed per network interface. One has been already taken by the module."
  }

  default = []
}

variable "extra_tags" {
  description = "Map of additional tags to add to module's resources"
  type        = map(string)
  default     = {}
}
