Terraform AWS ECS Fargate Module
================================

Terraform module to create following AWS resources:
- ECS service, associated Service Discovery and Security Group resources
- ECS task definition
- Subnets, Route Tables and Routes
- IAM role and policy for VPC flow log

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Outputs](#outputs)
- [Licence](#licence)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
At least following input variables must be provided. See [full list](variables.tf) of supported variables

| Name                   | Description                                   |
| ---------------------- | --------------------------------------------- |
| name                   | Common name - unique identifier               |
| vpc_id                 | ID of a VPC resource will be created in       |
| subnet_ids             | Subnet IDs service will be attached to        |
| cluster_name           | Existing Fargate cluster to deploy service to |
| service_name           | Name to give to Fargate service               |
| container_definitions  | Json-formatted definition of task containers  |
| task_cpu               | CPU units. See [documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html) |
| task_memory            | Memory units. See [documentation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/task-cpu-memory-error.html) |
| discovery_namespace_id | Add service discovery to enable inter-service communication |

## <a name="usage"></a> Usage
```hcl-terraform
module "network" {
  source     = "git::https://gitlab.com/HippoLab/terraform-modules/aws-network.git"
  name       = "${var.project_name} ${local.environment}"
  extra_tags = local.common_tags
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = lower("${var.project_name}-${var.environment}")
  capacity_providers = [
    "FARGATE",
    "FARGATE_SPOT"
  ]

  default_capacity_provider_strategy {
    capacity_provider = var.environment == "production" ? "FARGATE" : "FARGATE_SPOT"
    base              = 0
    weight            = 1
  }

  setting {
    name  = "containerInsights"
    value = "disabled"
  }
}

resource "aws_ecr_repository" "frontend" {
  name = "frontend"

  image_scanning_configuration {
    scan_on_push = true
  }
}

module "frontend_ecs" {
  source = "https://gitlab.com/HippoLab/terraform-modules/aws-fargate.git"
  
  name                   = "${var.project_name} ${local.environment}"
  vpc_id                 = module.network.vpc_id
  subnet_ids             = module.network.private_subnet_ids
  cluster_name           = aws_ecs_cluster.ecs_cluster.name
  platform_version      = "1.4.0"
  service_name           = "frontend"
  container_definitions  = templatefile("${path.root}/task_definitions/frontend.json", local.frontend_task_definition_vars)
  task_cpu               = 1024
  task_memory            = 2048
  load_balancers = [
    {
      container_name   = local.frontend_task_definition_vars["CONTAINER_NAME"],
      container_port   = 8080
      target_group_arn = module.frontend_alb.target_group_arn
    }
  ]
  desired_count          = 3
  discovery_namespace_id = aws_service_discovery_private_dns_namespace.local.id
  extra_tags             = local.common_tags
}

locals {
  frontend_task_definition_vars = {
    CONTAINER_REGISTRY = aws_ecr_repository.frontend.repository_url
    CONTAINER_NAME     = "frontend"
    CONTAINER_VERSION  = var.container_version
  }
}

resource "aws_security_group_rule" "frontend_lb_ingress" {
  security_group_id        = module.frontend_ecs.sg_id
  type                     = "ingress"
  protocol                 = "TCP"
  from_port                = 8080
  to_port                  = 8080
  source_security_group_id = module.frontend_alb.sg_id
  description              = "Frontend ALB"
}

module "frontend_alb" {
  source     = "git::https://gitlab.com/HippoLab/terraform-modules/aws-alb.git"
  name       = "${var.project_name} ${local.environment} Frontend"
  vpc_id     = module.network.vpc_id
  internal   = local.environment != "production" ? true : false
  subnet_ids = module.network.subnet_ids["public"]
  listeners = [
    {
      name     = "HTTP"
      protocol = "HTTP",
      port     = 80,
      action   = "redirect"
    },
    {
      name     = "HTTPS"
      protocol = "HTTPS",
      port     = 443,
      action   = "forward"
    }
  ]
  certificate_arn   = aws_acm_certificate_validation.environment_wildcard.certificate_arn
  health_check_port = 8080
  target_type       = "ip"
  access_logging = [
    {
      access_logs_bucket        = aws_s3_bucket.logging_bucket.id
      access_logs_bucket_prefix = "frontend_alb"
      logging_enabled           = true
    }
  ]
  extra_tags = local.common_tags
  depends_on = [
    aws_s3_bucket_policy.logging_bucket // Creation of an ALB fails if permissions are not granted before
  ]
}

resource "aws_security_group_rule" "frontend_alb_http_ingress" {
  security_group_id = module.frontend_alb.sg_id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 80
  to_port           = 80
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Internet"
}

resource "aws_security_group_rule" "frontend_alb_https_ingress" {
  security_group_id = module.frontend_alb.sg_id
  type              = "ingress"
  protocol          = "TCP"
  from_port         = 443
  to_port           = 443
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Internet"
}

resource "aws_security_group_rule" "alb_frontend_egress" {
  security_group_id        = module.frontend_alb.sg_id
  type                     = "egress"
  protocol                 = "TCP"
  from_port                = 8080
  to_port                  = 8080
  source_security_group_id = module.frontend_ecs.sg_id
}

resource "aws_service_discovery_private_dns_namespace" "local" {
  name        = lower("${local.environment}.${var.project_name}.local")
  description = "DNS discovery namespace"
  vpc         = module.network.vpc_id
  tags        = local.common_tags
}
```

## <a name="outputs"></a> Outputs
Full list of module outputs and their descriptions can be found in [outputs.tf](outputs.tf)

## <a name="licence"></a> Licence
The module is being distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru><br/>London
