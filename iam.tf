data "aws_iam_policy" "execution_policy" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

data "aws_iam_policy_document" "assume_role_policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ecs-tasks.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "execution_role" {
  name               = "${join("", regexall("[[:alnum:]]", var.name))}${title(var.service_name)}FargateExecutionRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource "aws_iam_role" "task_role" {
  name               = "${join("", regexall("[[:alnum:]]", var.name))}${title(var.service_name)}FargateTaskRole"
  assume_role_policy = data.aws_iam_policy_document.assume_role_policy.json
}

resource "aws_iam_role_policy_attachment" "execution_role_policy_attachment" {
  policy_arn = data.aws_iam_policy.execution_policy.arn
  role       = aws_iam_role.execution_role.name
}
